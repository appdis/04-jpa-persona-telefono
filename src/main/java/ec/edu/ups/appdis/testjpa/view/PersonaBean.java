package ec.edu.ups.appdis.testjpa.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import ec.edu.ups.appdis.testjpa.bussiness.PersonaBussiness;
import ec.edu.ups.appdis.testjpa.modelo.Persona;
import ec.edu.ups.appdis.testjpa.modelo.Telefono;

@ManagedBean
@ViewScoped
public class PersonaBean {
	
	@Inject
	private PersonaBussiness perBussiness;
	
	@Inject
	private FacesContext facesContext;
	
	private Persona newPersona;
	
	private String id;  		//Parametro para edicion
	
	private List<Persona> personas;
	
	private boolean editing;
	
	@PostConstruct
	public void init() {
		newPersona = new Persona();
		newPersona.addTelefono(new Telefono());
		editing = false;
		personas = perBussiness.getListadoPersona();
	}	

	
	public void loadData() {
		
		System.out.println("load data " + id);
		if(id==null)
			return;
		try {
			newPersona = perBussiness.getPersona(id);
			editing = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), "Error");
            facesContext.addMessage(null, m);
		}
	}
	public Persona getNewPersona() {
		return newPersona;
	}
	public void setNewPersona(Persona newPersona) {
		this.newPersona = newPersona;
	}
	public List<Persona> getPersonas() {
		return personas;
	}
	

	public String getId() {
		return id;
	}


	public void setId(String id) {
		System.out.println("id param " + id);
		this.id = id;
	}


	public boolean isEditing() {
		return editing;
	}

	public void setEditing(boolean editing) {
		this.editing = editing;
	}

	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}

	public String guardar() {
		System.out.println("editando " + editing);
		try {
			if(editing)
				perBussiness.actualizar(newPersona);
			else
				perBussiness.save(newPersona);
			System.out.println("Registro guardado");
			init();
			return "list-personas?faces-redirect=true";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error al guardar");
			e.printStackTrace();
			
			
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), "Error");
            facesContext.addMessage(null, m);
		}		
		return null;
	}
	
	public String eliminar(String cedula) {
		
		try {
			perBussiness.eliminar(cedula);
			System.out.println("Registro eliminado");
			return "list-personas?faces-redirect=true";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error al eliminar");
			e.printStackTrace();			
			
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), "Error");
            facesContext.addMessage(null, m);
		}		
		return null;
	}
	
	public String editar(Persona persona) {
		editing = true;
		System.out.println(persona);
		//newPersona = persona;
		return "create-persona?faces-redirect=true&id="+persona.getCedula();
	}
	
	
	public String addTelefono() {
		newPersona.addTelefono(new Telefono());
		return null;
	}
	
	
	
}
