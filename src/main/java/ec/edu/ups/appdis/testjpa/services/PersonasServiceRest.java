package ec.edu.ups.appdis.testjpa.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import ec.edu.ups.appdis.testjpa.bussiness.PersonaBussiness;
import ec.edu.ups.appdis.testjpa.modelo.Persona;

@Path("/personas")
public class PersonasServiceRest {

	@Inject
	private PersonaBussiness pBussiness;
	
	@GET
	@Path("/listado")
	@Produces("application/json")
	public List<Persona> getPersonas(){
		return pBussiness.getListadoPersona();
	}
	
	@POST
	@Path("/insertar")
	@Produces("application/json")
	@Consumes("application/json")
	public Response insertar(Persona persona) {

		Response.ResponseBuilder builder =  null;
		Map<String, String> data = new HashMap<>();
		
		try {
			pBussiness.save(persona);
			data.put("code", "1");
			data.put("message", "OK");
			builder = Response.status(Response.Status.OK).entity(data);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			data.put("code", "99");
			data.put("message", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(data);
		}
		
		return builder.build();		
	}
	
}
