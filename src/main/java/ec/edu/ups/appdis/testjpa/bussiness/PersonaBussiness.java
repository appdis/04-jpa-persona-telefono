package ec.edu.ups.appdis.testjpa.bussiness;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.appdis.testjpa.dao.PersonaDAO;
import ec.edu.ups.appdis.testjpa.modelo.Persona;

@Stateless
public class PersonaBussiness {

	@Inject
	private PersonaDAO dao;
	
	
	public boolean save(Persona persona) throws Exception {
		Persona aux = dao.read(persona.getCedula());
		if(aux!=null)
			throw new Exception("Persona ya existe");
		else
			dao.insert(persona);	
		
		return true;
	}
	
	public List<Persona> getListadoPersona(){
		return dao.getPersonas();
	}
	
	public void eliminar(String cedula) throws Exception {
		Persona aux = dao.read(cedula);
		if(aux==null)
			throw new Exception("Persona no existe");
		else
			dao.remove(cedula);
	}
	
	public Persona getPersona(String cedula) throws Exception {
		Persona aux = dao.read(cedula);
		if(aux==null)
			throw new Exception("Persona no existe");
		else
			return aux;
	}
	
	public void actualizar(Persona persona) throws Exception {
		Persona aux = dao.read(persona.getCedula());
		if(aux==null)
			throw new Exception("Persona no existe");
		else
			dao.update(persona);
	}
}
